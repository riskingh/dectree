#ifndef __DecTree__ReverseComparer__
#define __DecTree__ReverseComparer__

#include <stdio.h>

namespace nmx {
    template<class _T, class _Comparer = std::less<_T>>
    bool less(const _T &t1, const _T &t2, _Comparer _comparer = _Comparer()) {
        return _comparer(t1, t2);
    }
    
    template<class _T, class _Comparer = std::less<_T>>
    bool greater(const _T &t1, const _T &t2, _Comparer _comparer = _Comparer()) {
        return _comparer(t2, t1);
    }
    
    template<class _T, class _Comparer = std::less<_T>>
    bool equal(const _T &t1, const _T &t2, _Comparer _comparer = _Comparer()) {
        return !(_comparer(t1, t2) || _comparer(t2, t1));
    }
    
    template<class _Comparer>
    class CLessEqual {
    private:
        _Comparer lessComparer;
    public:
        CLessEqual(_Comparer _lessComparer)
        : lessComparer(_lessComparer) {}
        
        template<class _T>
        bool operator ()(const _T &_t1, const _T &_t2) {
            return nmx::less(_t1, _t2, lessComparer) || nmx::equal(_t1, _t2, lessComparer);
        }
    };
}

#endif /* defined(__DecTree__ReverseComparer__) */
