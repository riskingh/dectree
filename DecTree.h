#ifndef __DecTree__DecTree__
#define __DecTree__DecTree__

#include <stdio.h>
#include "Node.h"
#include "NodeFunctions.h"

namespace nmx {
    
    template<class _T, class _Comparer = std::less<_T>>
    class CDecTree {
    private:
        CNode<_T> *root;
        _Comparer lessComparer;
    public:
        CDecTree(_Comparer _comparer = _Comparer())
        : root(NULL), lessComparer(_comparer) {
        }
        
        void show() {
            if (root) root->show();
        }
        
        bool insert(const _T &_value) {
            CNode<_T> *leftPart, *middlePart, *rightPart;
            splitNode3(root, _value, leftPart, middlePart, rightPart, lessComparer);
            bool inserted = false;
            if (!middlePart) {
                inserted = true;
                middlePart = new CNode<_T>(_value);
            }
            root = mergeNodes3(leftPart, middlePart, rightPart);
            return inserted;
        }
        
        CNode<_T> *find(const _T &_value) {
            CNode<_T> *leftPart, *middlePart, *rightPart;
            splitNode3(root, _value, leftPart, middlePart, rightPart, lessComparer);
            root = mergeNodes3(leftPart, middlePart, rightPart);
            return middlePart;
        }
    };
}

#endif /* defined(__DecTree__DecTree__) */
