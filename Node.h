#ifndef __DecTree__Node__
#define __DecTree__Node__

#include <stdio.h>
#include <iostream>
#include <cstdlib>

#include "Comparers.h"

namespace nmx {
    
    template<class _T>
    class CNode {
    private:
        static int _ID;
        int _id;
        const int heapParam;
        CNode<_T> *left, *right;
        const _T value;
    public:
        CNode(const _T &value)
        : _id(_ID++), heapParam(rand()), left(NULL), right(NULL), value(value) {
        }
        
        void show();
        
        int getId();
        int getHeapParam();
        
        CNode<_T> *getLeft();
        CNode<_T> *getRight();
        
        const _T getValue();
        
        void setLeft(CNode<_T> *_left);
        void setRight(CNode<_T> *_right);
    };
    template<class _T>
    int CNode<_T>::_ID = 0;
    
    template<class _T>
    void CNode<_T>::show() {
        if (left) left->show();
        std::cout << _id << ": " << value << "\n";
        if (right) right->show();
    }
    
    template<class _T>
    int CNode<_T>::getId() {
        return _id;
    }
    
    template<class _T>
    int CNode<_T>::getHeapParam() {
        return heapParam;
    }
    
    template<class _T>
    CNode<_T> *CNode<_T>::getLeft() {
        return left;
    }
    
    template<class _T>
    CNode<_T> *CNode<_T>::getRight() {
        return right;
    }
    
    template<class _T>
    const _T CNode<_T>::getValue() {
        return value;
    }
    
    template<class _T>
    void CNode<_T>::setLeft(CNode<_T> *_left) {
        left = _left;
    }
    
    template<class _T>
    void CNode<_T>::setRight(CNode<_T> *_right) {
        right = _right;
    }
}

#endif /* defined(__DecTree__Node__) */
