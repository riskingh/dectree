#ifndef __DecTree__NodeFunctions__
#define __DecTree__NodeFunctions__

#include <stdio.h>
#include "Node.h"

namespace nmx {
    template<class _T>
    CNode<_T> *update(CNode<_T> *_node) {
        if (!_node) return NULL;
        //do something
        return _node;
    }
    
    template<class _T>
    CNode<_T> *mergeNodes(CNode<_T> *_leftNode, CNode<_T> *_rightNode) {
        if (!_leftNode) return _rightNode;
        if (!_rightNode) return _leftNode;
        if (_leftNode->getHeapParam() < _rightNode->getHeapParam()) {
            _leftNode->setRight(mergeNodes(_leftNode->getRight(), _rightNode));
            return update(_leftNode);
        }
        else {
            _rightNode->setLeft(mergeNodes(_leftNode, _rightNode->getLeft()));
            return update(_rightNode);
        }
    }
    
    template<class _T>
    CNode<_T> *mergeNodes3(CNode<_T> *_leftPart, CNode<_T> *_middlePart, CNode<_T> *_rightPart) {
        return mergeNodes(mergeNodes(_leftPart, _middlePart), _rightPart);
    }
    
    template<class _T, class _Comparer>
    void splitNode(CNode<_T> *_node, const _T &_value, CNode<_T> *&_leftPart, CNode<_T> *&_rightPart, _Comparer _comparer) {
        if (!_node) {
            _leftPart = _rightPart = NULL;
            return;
        }
        if (_comparer(_node->getValue(), _value)) {
            splitNode(_node->getRight(), _value, _leftPart, _rightPart, _comparer);
            _node->setRight(_leftPart);
            _leftPart = update(_node);
        }
        else {
            splitNode(_node->getLeft(), _value, _leftPart, _rightPart, _comparer);
            _node->setLeft(_rightPart);
            _rightPart = update(_node);
        }
    }
    
    template<class _T, class _Comparer>
    void splitNodeL(CNode<_T> *_node, const _T &_value, CNode<_T> *&_leftPart, CNode<_T> *&_rightPart, _Comparer _lessComparer) {
        splitNode(_node, _value, _leftPart, _rightPart, CLessEqual<_Comparer>(_lessComparer));
    }
    
    template<class _T, class _Comparer>
    void splitNodeR(CNode<_T> *_node, const _T &_value, CNode<_T> *&_leftPart, CNode<_T> *&_rightPart, _Comparer _lessComparer) {
        splitNode(_node, _value, _leftPart, _rightPart, _lessComparer);
    }
    
    template<class _T, class _Comparer>
    void splitNode3(CNode<_T> *_node, const _T &_value, CNode<_T> *&_leftPart, CNode<_T> *&_middlePart, CNode<_T> *&_rightPart, _Comparer _lessComparer) {
        splitNodeL(_node, _value, _leftPart, _rightPart, _lessComparer);
        splitNodeR(_leftPart, _value, _leftPart, _middlePart, _lessComparer);
    }
}

#endif /* defined(__DecTree__NodeFunctions__) */
